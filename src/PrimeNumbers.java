
import java.util.Scanner;

public class PrimeNumbers {
	private int numberOfPrimes;
	private int primes[];
	private Scanner input;
	
	public PrimeNumbers() {
		input = new Scanner(System.in);
	}
	
	public void getNumberFromUser() {
		System.out.print("How many prime numbers do you want? ");
		numberOfPrimes = input.nextInt();
		if(numberOfPrimes <= 0) {
			System.out.println("You need to generate at least one prime number");
			getNumberFromUser();
		}
		primes = new int[numberOfPrimes];
	}
	
	public void generatePrimes() {
		primes[0] = 2;
		int primeNumbers = 1;
		int current = 3;
		
		while(primeNumbers < numberOfPrimes) {
			boolean isPrime = true;
			int previousPrime = 0;
			while(previousPrime < primeNumbers) {
				if(current%primes[previousPrime] == 0) {
					isPrime = false;
				}
				previousPrime = previousPrime + 1;
			}
			
			if(isPrime) {
				primes[primeNumbers] = current;
				primeNumbers = primeNumbers + 1;
			}
			
			current = current + 2;
		}
		
		
	}
	
	
	public void printPrimes() {
		int i;
		
		for(i = 1; i <= numberOfPrimes; i++) {
			System.out.print(i+"\t"+primes[i-1]+"\n");
		}
	}

}
